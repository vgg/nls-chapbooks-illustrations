# Export LISA project annotations into COCO annotation format containing the full
# training set (for training the illustration detector model)
#
# Author: Abhishek Dutta
# Date: 06-Sep-2021
#
#
# Updates:
# - append update log here in the format: Date : Author : Comment


import json
from json import encoder
import random
import os
import datetime
import argparse
import sys

FLOAT_PRECISION = 2
POSITIVE_COUNT = 3629
NEGATIVE_COUNT = 43700

def lisa_export(lisa_project_fn, coco_fn):
    with open(lisa_project_fn) as f:
        project = json.load(f)

    # collect a list of all file_indices that contain a region
    positive_findex_list = []
    negative_findex_list = []
    for findex, file in enumerate(project['files']):
        if len(project['files'][findex]['regions']):
            positive_findex_list.append(findex)
        else:
            negative_findex_list.append(findex)
    print('total: positive=%d, negative=%d' % (len(positive_findex_list), len(negative_findex_list)))
    ## Training set should contain equal number of positive and negative examples
    ## this is to avoid biasing the training. Therefore, we randomly shuffle the negative
    ## samples and select the number of negative samples same as the number of positive
    ## samples.
    random.shuffle(negative_findex_list)
    training_negative_findex_list = negative_findex_list[0:len(positive_findex_list)]
    training_findex_list = positive_findex_list + training_negative_findex_list
    print('training set: positive=%d, negative=%d' % (len(positive_findex_list), len(training_findex_list)))

    unique_annotaion_id = 1
    coco = init_coco_template()
    coco['categories'].append( {'id':1, 'name':'early_printed_illustration', 'supercategory':'illustration'} )
    for i in range(0, len(training_findex_list)):
        findex = training_findex_list[i]
        coco['images'].append({
                'id':findex,
                'width':project['files'][findex]['fdata']['width'],
                'height':project['files'][findex]['fdata']['height'],
                'file_name':project['files'][findex]['src'],
                'license':0,
                'flickr_url':'',
                'coco_url':'',
                'date_captured':''
        })
        for rindex in range(0, len(project['files'][findex]['regions'])):
            coco['annotations'].append({
                    'id':unique_annotaion_id,
                    'image_id':findex,
                    'category_id':1,
                    'segmentation':[ bbox_to_segmentation(project['files'][findex]['regions'][rindex]) ],
                    'area':bbox_to_area(project['files'][findex]['regions'][rindex]),
                    'bbox':project['files'][findex]['regions'][rindex],
                    'iscrowd':0
            });
            unique_annotaion_id = unique_annotaion_id + 1
    # write coco to json file
    with open(coco_fn, 'w') as f:
        json.dump(coco, f)
        print('  Written COCO dataset to %s' % (coco_fn))
        
    
def init_coco_template():
    coco = { 'info':{}, 'images':[], 'annotations':[], 'licenses':[], 'categories':[] };
    coco['info'] = { 'year': 2021,
                     'version': '1.0',
                     'description': 'Manual Annotations',
                     'contributor': 'Abhishek Dutta',
                     'url': 'http://www.robots.ox.ac.uk/~vgg/software/lisa/',
                     'date_created': str(datetime.datetime.now()),
    };
    coco['licenses'] = [ {'id':0, 'name':'Unknown License', 'url':''} ] # indicates that license is unknown
    return coco

def bbox_to_segmentation(bbox):
    if len(bbox) == 4:
        bbox[0] = round(bbox[0], FLOAT_PRECISION)
        bbox[1] = round(bbox[1], FLOAT_PRECISION)
        bbox[2] = round(bbox[2], FLOAT_PRECISION)
        bbox[3] = round(bbox[3], FLOAT_PRECISION)
        return [ bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3], bbox[0], bbox[1]+bbox[3] ]
    else:
        return bbox

def bbox_to_area(bbox):
    if len(bbox) == 4:
        return round(bbox[2] * bbox[3], FLOAT_PRECISION)
    else:
        return -1

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Convert manual annotations from a LISA project to COCO format")
    parser.add_argument("--lisa_project_fn",
                        required=True,
                        type=str,
                        help="location of LISA project containing annotations")
    parser.add_argument("--coco_fn",
                        required=True,
                        type=str,
                        help="COCO annotations are exported to this file")

    args = parser.parse_args()
    lisa_export(args.lisa_project_fn, args.coco_fn)

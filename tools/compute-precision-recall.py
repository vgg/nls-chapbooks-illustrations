# Merge two LISA projects
#
# Author: Abhishek Dutta
# Date: 04 Dec. 2020
#
# Run using: $python3 merge_projects.py
#
# Updates:
# - append update log here in the format: Date : Author : Comment


import json
from json import encoder
import random
import os
import datetime
import argparse

FLOAT_PRECISION = 2

def lisa_compute_precision_recall(detections_fn, gndtruth_fn, findex0, findex1, iou_threshold):
  with open(detections_fn, 'r') as f1:
    detections = json.load(f1)
  with open(gndtruth_fn, 'r') as f2:
    gndtruth = json.load(f2)

  detections_filecount = len(detections['files'])
  gndtruth_filecount = len(gndtruth['files'])
  if detections_filecount != gndtruth_filecount:
    print('WARNING: input LISA project have different number of files: %d/%d' %(detections_filecount, gndtruth_filecount))

  true_positive_count  = 0
  false_positive_count = 0
  false_negative_count = 0
  total_detections = 0
  for findex in range(findex0, findex1, 1):
    src = gndtruth['files'][findex]['src']
    if src != detections['files'][findex]['src']:
      print('ERROR: filename mismatch : %s : %s' % (src, lisa2['files'][findex]['src']))
      break
    gndtruth_region_count = len(gndtruth['files'][findex]['regions'])
    detected_region_count = len(detections['files'][findex]['regions'])
    if gndtruth_region_count == 0:
      continue

    regions_found = 0
    for grindex in range(0, gndtruth_region_count):
      gregion = gndtruth['files'][findex]['regions'][grindex]
      max_iou = 0
      max_iou_drindex = -1
      #print('[file=%d] %s : [%d] %s' % (findex, src, grindex, json.dumps(gregion)))

      for drindex in range(0, detected_region_count):
        dregion = detections['files'][findex]['regions'][drindex]
        iou = compute_iou(gregion, dregion)
        #print('  [%d] det=%s, iou=%.4f' % (drindex, json.dumps(dregion), iou))
        region_found = False
        if iou >= iou_threshold:
          max_iou = iou
          max_iou_drindex = drindex
          region_found = True
        if region_found:
          true_positive_count += 1
          regions_found += 1
        else:
          false_negative_count += 1
    false_positive_count += (detected_region_count - regions_found)

  precision = true_positive_count / (true_positive_count + false_positive_count) # how many correct detections in all your detections?
  recall = true_positive_count / (true_positive_count + false_negative_count) # out of total ground truth regions, how many region can your recover?
  #print('iou_threshold=%.2f, precision=%.3f, recall=%.3f, true positive=%d, false positive=%d, false negative=%d' % (iou_threshold, precision, recall, true_positive_count, false_positive_count, false_negative_count))
  print('IoU threshold=%.2f, precision=%.3f, recall=%.3f' % (iou_threshold, precision, recall))

def compute_iou(r1, r2):
  r1x0 = r1[0]
  r1y0 = r1[1]
  r1x1 = r1[0] + r1[2]
  r1y1 = r1[1] + r1[3]

  r2x0 = r2[0]
  r2y0 = r2[1]
  r2x1 = r2[0] + r2[2]
  r2y1 = r2[1] + r2[3]

  # compute coordinates of the intersecting rectangle
  x0 = max(r1x0, r2x0)
  y0 = max(r1y0, r2y0)
  x1 = min(r1x1, r2x1)
  y1 = min(r1y1, r2y1)

  intersection_area = max(0, x1 - x0 + 1) * max(0, y1 - y0 + 1)
  area1 = (r1x1 - r1x0 + 1) * (r1y1 - r1y0 + 1)
  area2 = (r2x1 - r2x0 + 1) * (r2y1 - r2y0 + 1)
  total_area = float(area1 + area2 - intersection_area)
  iou = intersection_area / total_area
  return iou

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Show diff between two LISA projects')
  parser.add_argument('detections', type=str,
                      help='LISA project filename containing automatically detected bounding box annotations')
  parser.add_argument('gndtruth', type=str,
                      help='LISA project filename containing ground truth bounding box annotations')
  parser.add_argument('findex0', type=int,
                      help='only files in the [findex0, findex1) range are included in analysis')
  parser.add_argument('findex1', type=int,
                      help='only files in the [findex0, findex1) range are included in analysis')

  args = parser.parse_args()

  lisa_compute_precision_recall(args.detections, args.gndtruth,
                                args.findex0, args.findex1, 0.5)
  lisa_compute_precision_recall(args.detections, args.gndtruth,
                                args.findex0, args.findex1, 0.75)
  lisa_compute_precision_recall(args.detections, args.gndtruth,
                                args.findex0, args.findex1, 0.95)

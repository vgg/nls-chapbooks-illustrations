# Export LISA project in COCO annotation format
#
# Author: Abhishek Dutta
# Date: 19 June 2020
#
# Run using: $python3 lisa_to_coco.py --lisa_project_fn=lisa_project_5a1b_20200618_15h33m.json
#
# Updates:
# - append update log here in the format: Date : Author : Comment


import json
from json import encoder
import random
import os
import datetime
import argparse
import sys

FLOAT_PRECISION = 2
#SUBSET_LIST = { 'train': 0.80, 'valid':0.15 }
SUBSET_LIST = { 'train': 1.0 }

def lisa_export(lisa_project_fn):
  with open(lisa_project_fn) as f:
    project = json.load(f)

  # collect a list of all file_indices that contain a region
  sel_findex_list = []
  for findex, file in enumerate(project['files']):
    if len(project['files'][findex]['regions']):
      sel_findex_list.append(findex)

  N = len(sel_findex_list)
  print('Exporting annotations in %d images to COCO format' % N)
  start_index = 0;
  random.seed(9972)
  random.shuffle(sel_findex_list)

  for subset in SUBSET_LIST:
    end_index = start_index + int(SUBSET_LIST[subset] * N)
    if end_index > N:
      end_index = N
    print('%s : %d to %d :' % (subset, start_index, end_index))
    unique_annotaion_id = 1
    coco = init_coco_template()
    coco['categories'].append( {'id':1, 'name':'early_printed_illustration', 'supercategory':'illustration'} )
    for i in range(start_index, end_index, 1):
      findex = sel_findex_list[i]
      coco['images'].append({
        'id':findex,
        'width':project['files'][findex]['fdata']['width'],
        'height':project['files'][findex]['fdata']['height'],
        'file_name':project['files'][findex]['src'],
        'license':0,
        'flickr_url':'',
        'coco_url':'',
        'date_captured':''
      })
      for rindex in range(0, len(project['files'][findex]['regions'])):
        coco['annotations'].append({
          'id':unique_annotaion_id,
          'image_id':findex,
          'category_id':1,
          'segmentation':[ bbox_to_segmentation(project['files'][findex]['regions'][rindex]) ],
          'area':bbox_to_area(project['files'][findex]['regions'][rindex]),
          'bbox':project['files'][findex]['regions'][rindex],
          'iscrowd':0
        });
        unique_annotaion_id = unique_annotaion_id + 1
    # write coco to json file
    outdir = os.path.dirname(lisa_project_fn)
    prefix = os.path.splitext(lisa_project_fn)[0]
    coco_fn = os.path.join(outdir, prefix + '_' + subset + '_coco.json')
    with open(coco_fn, 'w') as f:
      json.dump(coco, f)
      print('Written COCO dataset to %s' % (coco_fn))
    start_index = end_index


def init_coco_template():
  coco = { 'info':{}, 'images':[], 'annotations':[], 'licenses':[], 'categories':[] };
  coco['info'] = { 'year': 2021,
                   'version': '1.0',
                   'description': 'Manual Annotations',
                   'contributor': 'Abhishek Dutta',
                   'url': 'http://www.robots.ox.ac.uk/~vgg/software/lisa/',
                   'date_created': str(datetime.datetime.now()),
                 };
  coco['licenses'] = [ {'id':0, 'name':'Unknown License', 'url':''} ] # indicates that license is unknown
  return coco

def bbox_to_segmentation(bbox):
  if len(bbox) == 4:
    bbox[0] = round(bbox[0], FLOAT_PRECISION)
    bbox[1] = round(bbox[1], FLOAT_PRECISION)
    bbox[2] = round(bbox[2], FLOAT_PRECISION)
    bbox[3] = round(bbox[3], FLOAT_PRECISION)
    return [ bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3], bbox[0], bbox[1]+bbox[3] ]
  else:
    return bbox

def bbox_to_area(bbox):
  if len(bbox) == 4:
    return round(bbox[2] * bbox[3], FLOAT_PRECISION)
  else:
    return -1

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Convert manual annotations from a LISA project to COCO format")
    parser.add_argument("--lisa_project_fn",
                        required=True,
                        type=str,
                        help="location of LISA project containing annotations")
    args = parser.parse_args()
    lisa_export(args.lisa_project_fn)

# Export LISA project annotations into COCO annotation format and split them into
# training and test set for cross validation experiment
#
# Author: Abhishek Dutta
# Date: 02-Sep-2021
#
# Run using: $python3 lisa-to-k-fold-cross-validation-coco-dataset.py --lisa_project_fn=... --outdir=...
#
# Updates:
# - append update log here in the format: Date : Author : Comment

import json
from json import encoder
import random
import os
import datetime
import argparse
import sys
import numpy as np

FLOAT_PRECISION = 2
TOTAL_IMG_COUNT = 47329  # total image count in NLS Chapbooks dataset
POSITIVE_COUNT = 3629  # i.e. image containing an illustration
NEGATIVE_COUNT = TOTAL_IMG_COUNT - POSITIVE_COUNT  # i.e. image without any illustrations
SPLIT_NAME_LIST = ['train', 'test']

def lisa_export(lisa_project_fn, base_outdir, kfold):
    with open(lisa_project_fn) as f:
        project = json.load(f)
        # collect a list of all file_indices that contain a region
    positive_findex_list = np.ndarray((POSITIVE_COUNT), dtype=np.uint32)
    negative_findex_list = np.ndarray((NEGATIVE_COUNT), dtype=np.uint32)
    pfindex = 0
    nfindex = 0
    for findex, file in enumerate(project['files']):
        if len(project['files'][findex]['regions']):
            positive_findex_list[pfindex] = findex
            pfindex += 1
        else:
            negative_findex_list[nfindex] = findex
            nfindex += 1
    assert (pfindex == POSITIVE_COUNT), "unexpected number of images containing illustration"
    assert (nfindex == NEGATIVE_COUNT), "unexpected number of images without any illustrations"
    print('Before')
    print(positive_findex_list[0:10])
    print(negative_findex_list[0:10])
    np.random.shuffle(positive_findex_list)
    np.random.shuffle(negative_findex_list)
    print('After')
    print(positive_findex_list[0:10])
    print(negative_findex_list[0:10])

    ## Note:
    ## Training set should contain equal number of positive and negative examples
    ## this is to avoid biasing the training.
    ## The test set can contain an unbalanced set of positive and negative examples
    ## but the test set imbalance should be consistent across all k-folds

    positive_group_size = int(len(positive_findex_list) / kfold)
    print('positive_group_size=%d' % (positive_group_size))
    
    for k in range(0, kfold):
        positive0 = k * positive_group_size
        positive1 = (k+1) * positive_group_size
        test_set_positive = positive_findex_list[positive0:positive1]
        train_set_positive = np.setdiff1d(positive_findex_list, test_set_positive, assume_unique=True)

        negative0 = k * train_set_positive.shape[0]
        negative1 = (k+1) * train_set_positive.shape[0]
        train_set_negative = negative_findex_list[negative0:negative1]
        test_set_negative = np.setdiff1d(negative_findex_list, train_set_negative, assume_unique=True)

        split_findex_list = {
            'test':np.union1d(test_set_positive, test_set_negative),
            'train': np.union1d(train_set_positive, train_set_negative)
        }

        outdir = os.path.join(base_outdir, 'fold-%d' % (k), 'coco')
        if not os.path.exists(outdir):
            os.makedirs(outdir)

        for split_index, split_name in enumerate(SPLIT_NAME_LIST):
            coco_fn = os.path.join(outdir, '%s.json' % (split_name))
            save_coco_dataset(project, split_findex_list[split_name], coco_fn)
            print('k=%d : %s = %d : %s' % (k, split_name, len(split_findex_list[split_name]), coco_fn))

        #print(train_set)
        #print(test_set)

def save_coco_dataset(project, findex_list, coco_fn):
    unique_annotaion_id = 1
    coco = init_coco_template()
    coco['categories'].append( {'id':1, 'name':'Chapbook-Illustration', 'supercategory':'Chapbook-Illustration'} )
    for i in range(0, len(findex_list)):
        findex = findex_list[i]
        coco['images'].append({
                'id':int(findex),
                'width':project['files'][findex]['fdata']['width'],
                'height':project['files'][findex]['fdata']['height'],
                'file_name':project['files'][findex]['src'],
                'license':0,
                'flickr_url':'',
                'coco_url':'',
                'date_captured':''
        })
        for rindex in range(0, len(project['files'][findex]['regions'])):
            coco['annotations'].append({
                    'id':unique_annotaion_id,
                    'image_id':int(findex),
                    'category_id':1,
                    'segmentation':[ bbox_to_segmentation(project['files'][findex]['regions'][rindex]) ],
                    'area':bbox_to_area(project['files'][findex]['regions'][rindex]),
                    'bbox':project['files'][findex]['regions'][rindex],
                    'iscrowd':0
                });
            unique_annotaion_id = unique_annotaion_id + 1
    # write coco to json file
    with open(coco_fn, 'w') as f:
        json.dump(coco, f)

def init_coco_template():
    coco = { 'info':{}, 'images':[], 'annotations':[], 'licenses':[], 'categories':[] };
    coco['info'] = { 'year': 2021,
                     'version': '1.0',
                     'description': 'Manual Annotations',
                     'contributor': 'Abhishek Dutta',
                     'url': 'http://www.robots.ox.ac.uk/~vgg/software/lisa/',
                     'date_created': str(datetime.datetime.now()),
    };
    coco['licenses'] = [ {'id':0, 'name':'Unknown License', 'url':''} ] # indicates that license is unknown
    return coco

def bbox_to_segmentation(bbox):
    if len(bbox) == 4:
        bbox[0] = round(bbox[0], FLOAT_PRECISION)
        bbox[1] = round(bbox[1], FLOAT_PRECISION)
        bbox[2] = round(bbox[2], FLOAT_PRECISION)
        bbox[3] = round(bbox[3], FLOAT_PRECISION)
        return [ bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3], bbox[0], bbox[1]+bbox[3] ]
    else:
        return bbox

def bbox_to_area(bbox):
    if len(bbox) == 4:
        return round(bbox[2] * bbox[3], FLOAT_PRECISION)
    else:
        return -1

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Convert manual annotations from a LISA project to COCO format")
    parser.add_argument("--lisa_project_fn",
                        required=True,
                        type=str,
                        help="location of LISA project containing annotations")
    parser.add_argument("--base_outdir",
                        required=True,
                        type=str,
                        help="k-fold dataset are generated and stored in this folder")
    parser.add_argument("--kfold",
                        required=True,
                        type=int,
                        help="number of partitions (i.e. value of k in k-fold cross validation)")

    args = parser.parse_args()
    lisa_export(args.lisa_project_fn, args.base_outdir, args.kfold)

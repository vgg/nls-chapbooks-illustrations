# Export specifications (e.g. set id, filename, etc.) of visual group
#
# Author: Abhishek Dutta
# Date  : 06-July-2021

import csv
import os
import sqlite3

DATADIR = '../data/copy-of-vise-project-data/'
vgroup_spec_fn = os.path.join(DATADIR, 'illustration-group-specifications.csv')
nonmatch_list_fn = os.path.join(DATADIR, 'illustration-group-non-match.csv')

metadata_db_fn = os.path.join(DATADIR, 'metadata_db.sqlite')
metadata_db = sqlite3.connect(metadata_db_fn)
metadata_db.row_factory = sqlite3.Row
metadata_db_cursor = metadata_db.cursor()

group_db_fn = os.path.join(DATADIR, 'illustration-group.sqlite')
group_db = sqlite3.connect(group_db_fn)
group_db.row_factory = sqlite3.Row
group_db_vgroup_cursor = group_db.cursor()
group_db_region_cursor = group_db.cursor()

fileid_filename_map = {}
for row in metadata_db_cursor.execute('SELECT file_id, filename from file_metadata'):
  fid = row[0]
  filename = row[1]
  fileid_filename_map[fid] = filename

with open(vgroup_spec_fn, 'w') as f:
  f.write('project_id,visual_group_id,set_id,file_id,region_index,filename\n')
  for row in group_db_vgroup_cursor.execute('SELECT * from vgroup'):
    set_id = row[0]
    set_size = row[1]
    member_id_list_str = row[2]
    member_id_list = member_id_list_str.split(',')
    for region_id_str in member_id_list:
      for vgroup_region_row in group_db_region_cursor.execute('SELECT * from vgroup_region WHERE region_id = %s;' % (region_id_str)):
        file_id_str = vgroup_region_row[1]
        file_id = int(file_id_str)
        region_index = vgroup_region_row[2]
        filename = fileid_filename_map[file_id]
        f.write('nls-chapbooks-illustrations,illustration-group,%s,%s,%s,"%s"\n' % (set_id, file_id,region_index,filename))


with open(nonmatch_list_fn, 'w') as f:
  f.write('project_id,visual_group_id,file_id,region_id,filename\n')
  for row in group_db_vgroup_cursor.execute('SELECT query_id from vgroup_non_match'):
    region_id_str = int(row[0])
    for vgroup_region_row in group_db_region_cursor.execute('SELECT * from vgroup_region WHERE region_id = %s;' % (region_id_str)):
      file_id_str = vgroup_region_row[1]
      file_id = int(file_id_str)
      region_index = vgroup_region_row[2]
      filename = fileid_filename_map[file_id]
      f.write('nls-chapbooks-illustrations,illustration-group,%s,%s,"%s"\n' % (file_id, region_index, filename))

## Split manual annotations into two parts (training and testing) for learning exercises
##
## Author:  Abhishek Dutta <adutta@robots.ox.ac.uk>
## Date:    2022-Jul-19

import argparse
import json
import random
import os
import shutil
import time
import uuid

def init_lisa_project():
    lisa = {
        'project': {
            'shared_fid': '__FILE_ID__',
            'shared_rev': '__FILE_REV_ID__',
            'shared_rev_timestamp': '__FILE_REV_TIMESTAMP__',
            'project_name': '',
            'project_id': str(uuid.uuid4()),
            'creator': 'VGG NLS Chapbooks Project (https://gitlab.com/vgg/nls-chapbooks-illustrations/)',
            'editor': 'List Annotator - LISA (https://gitlab.com/vgg/lisa)',
            'file_format_version': '0.0.3',
            'created_timestamp': time.time() * 1000
        },
        'config': {
            'file_src_prefix': '',
            'navigation_from': 0,
            'navigation_to': -1,
            'item_per_page': -1,
            'float_precision': 4,
            'item_height_in_pixel': 300,
            'show_attributes': {
                'file': [],
                'region': []
            }
        },
        'attributes': {
            'file': {
                'width': {
                    'aname': 'Width',
                    'atype': 'text'
                },
                'height': {
                    'aname': 'Height',
                    'atype': 'text'
                }
            },
            'region': {}
        },
        'files': []
    }
    return lisa

def main():
    ## initialize and define the command line parser
    parser = argparse.ArgumentParser(prog='manual-annotations-split',
                                     description='Split manual annotations into two parts (training and testing) for exercise')
    parser.add_argument("--train-samples",
                        required=True,
                        type=int,
                        help="number of annotated samples in the training set")
    parser.add_argument("--test-samples",
                        required=True,
                        type=int,
                        help="number of annotated samples in the test set")
    parser.add_argument("--annotations-lisa-fn",
                        required=False,
                        type=str,
                        help="LISA project filename containing all the manual annotations")
    parser.add_argument("--img-dir",
                        required=True,
                        type=str,
                        help="location of NLS chapbooks dataset images")
    parser.add_argument("--out-dir",
                        required=True,
                        type=str,
                        help="images from training and test set will be copied into this folder")
    parser.add_argument("--rand-seed",
                        required=False,
                        type=int,
                        default=9973,
                        help="random seed (to reproduce a training and testing set)")

    args = parser.parse_args()

    lisa = {}
    with open(args.annotations_lisa_fn, 'r') as f:
        lisa = json.load(f)

    # create a list of findex that contains a manually annotated region
    annotated_findex_list = []
    for findex in range(0, len(lisa['files'])):
        if len(lisa['files'][findex]['regions']):
            annotated_findex_list.append(findex)
    print('Number of annotated files = %d' % (len(annotated_findex_list)))

    total_samples = args.train_samples + args.test_samples

    random.seed(args.rand_seed)
    random.shuffle(annotated_findex_list)
    ## allows one to keep same test set while increase the
    ## size of training set
    test_findex_list = annotated_findex_list[0:args.test_samples]
    train_findex_list = annotated_findex_list[args.test_samples: args.test_samples + args.train_samples]

    if not os.path.exists(args.out_dir):
        os.mkdir(args.out_dir)
    out_img_dir = os.path.join(args.out_dir, 'img')
    if not os.path.exists(out_img_dir):
        os.mkdir(out_img_dir)
    train_fn = os.path.join(args.out_dir, 'train.json')
    test_fn = os.path.join(args.out_dir, 'test.json')

    ## save training set
    train = init_lisa_project()
    train['project']['project_name'] = 'train'
    train['config']['file_src_prefix'] = 'img/'

    train_file_id = 1
    for findex in train_findex_list:
        src_fn = os.path.join(args.img_dir, lisa['files'][findex]['src'])
        tgt_fn = 'train-%.4d.jpg' % (train_file_id)
        tgt_path = os.path.join(out_img_dir, tgt_fn)
        shutil.copy2(src_fn, tgt_path)

        file_metadata = lisa['files'][findex]
        file_metadata['src'] = tgt_fn
        train['files'].append(file_metadata)
        train_file_id += 1

    with open(train_fn, 'w') as f:
        json.dump(train, f)
        print('saved ' + train_fn)

    ## save testing set
    test = init_lisa_project()
    test['project']['project_name'] = 'test'
    test['config']['file_src_prefix'] = 'img/'

    test_file_id = 1
    for findex in test_findex_list:
        src_fn = os.path.join(args.img_dir, lisa['files'][findex]['src'])
        tgt_fn = 'test-%.4d.jpg' % (test_file_id)
        tgt_path = os.path.join(out_img_dir, tgt_fn)
        shutil.copy2(src_fn, tgt_path)

        file_metadata = lisa['files'][findex]
        file_metadata['src'] = tgt_fn
        test['files'].append(file_metadata)
        test_file_id += 1

    with open(test_fn, 'w') as f:
        json.dump(test, f)
        print('saved ' + test_fn)


if __name__ == '__main__':
    main()

#!/bin/bash

# Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
# Date  : 03-Sep-2021

export MYSTORE=$HOME/nls
source $MYSTORE/venv/bin/activate
KFOLD=10
KFOLD_DIR="${MYSTORE}/cross-validation-${KFOLD}-fold"

if [ ! -d $KFOLD_DIR ]; then
    mkdir -p $KFOLD_DIR
fi

## Generate COCO dataset for each fold
if [ ! -d "${KFOLD_DIR}/fold-0/coco/" ]; then
    cd $MYSTORE/nls-chapbooks-illustrations/tools/
    python lisa-to-k-fold-cross-validation-coco-dataset.py \
       --lisa_project_fn=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step5-manual-verification-image-20000-to-47329.json \
       --base_outdir=$KFOLD_DIR \
       --kfold=10
fi

## create tensorflow record for coco dataset
if [ ! -d "${KFOLD_DIR}/fold-0/tfrecord/" ]; then
    echo "Exporting to tensorflow record ..."
    cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
    for i in $(seq 1 $KFOLD); do
	k=$(expr $i - 1)
	COCO_DIR="${KFOLD_DIR}/fold-${k}/coco"
	TFRECORD_DIR="${KFOLD_DIR}/fold-${k}/tfrecord"
	echo "creating ${TFRECORD_DIR}"
	mkdir $TFRECORD_DIR

	PYTHONPATH=".:$PYTHONPATH" python dataset/create_coco_tfrecord.py --logtostderr \
		  --image_dir=$MYSTORE/nls-data-chapbooks/ \
		  --object_annotations_file=$COCO_DIR/train.json \
		  --output_file_prefix=$TFRECORD_DIR/train \
		  --num_shards=8

	PYTHONPATH=".:$PYTHONPATH" python dataset/create_coco_tfrecord.py --logtostderr \
		  --image_dir=$MYSTORE/nls-data-chapbooks/ \
		  --object_annotations_file=$COCO_DIR/test.json \
		  --output_file_prefix=$TFRECORD_DIR/test \
		  --num_shards=32
    done
fi

## run each iteration of k-fold cross validation
for i in $(seq 1 $KFOLD); do
    k=$(expr $i - 1)
    echo "Processing iteration ${k} ..."
    TFRECORD_DIR="${KFOLD_DIR}/fold-${k}/tfrecord"
    MODEL_DIR="${KFOLD_DIR}/fold-${k}/model"
    EVAL_DIR="${KFOLD_DIR}/fold-${k}/model/eval"
    cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/

    if [ ! -d $MODEL_DIR ]; then
	python main.py --mode=train \
	   --train_file_pattern=$TFRECORD_DIR/train-*-of-00008.tfrecord \
	   --model_name=efficientdet-d0 \
	   --model_dir=$MODEL_DIR/  \
	   --ckpt=efficientdet-d0 \
	   --train_batch_size=8 \
	   --num_examples_per_epoch=6534 --num_epochs=10  \
	   --hparams=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/hparams.yaml \
	   --eval_after_train=false --tf_random_seed=9973
    fi

    if [ ! -d $EVAL_DIR ]; then
	python main.py --mode=eval \
	   --val_file_pattern=$TFRECORD_DIR/test-*-of-00032.tfrecord \
	   --model_name=efficientdet-d0 \
	   --model_dir=$MODEL_DIR/ \
	   --eval_batch_size=8 \
	   --eval_timeout=10 \
	   --eval_samples=40795 \
	   --hparams=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/hparams.yaml \
	   --tf_random_seed=9973
    fi
done
